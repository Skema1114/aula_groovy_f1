package aula

class Exercicio01 {
	static main(args){
		println "Ola mundo groovy"
		
		String nome = "Rafael"
		int idade = 22
		
		String frase = "O "+nome+" tem "+idade+" anos"
		println frase
		
		String frase2 = "O $nome tem $idade anos"
		println frase2
	}
}
