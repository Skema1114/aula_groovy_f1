package aula

import static org.junit.Assert.*
import org.junit.Test
import classes.Cliente
import classes.Comida;
import classes.Conexao;
import classes.Funcionario;
import classes.Funcionario2
import classes.Nota
import classes.Pedido
import classes.Produto
import classes.Somar;
import classes.Teste
import classes.Venda;
import classes.Cliente as Xu
import static javax.swing.JFrame.EXIT_ON_CLOSE as ex


// PARA EXECUTAR OS TESTES BASTA ESCOLHER O NOME DA CLASSE DO EXERCICIO E EXECUTAR COM A MESMA SELECIONADA
class Exercicios {
	// DOCUMENTA��O: http://www.groovy-lang.org
	
	@Test
	void exercicio1ponto2() {	
		int a = 10
		
		println a.class
		print a.toString()
		println 10.toString()
		
		println 12l.class.name
		println 11g.class.name
		
		BigDecimal valor = 200.50
		print valor
	}
	
	@Test
	void exercicio02(){
		// POGO (JAVA � POJO)
		
		Cliente c = new Cliente()
		
		c.setNome "Rafael"
		c.setData new Date()
		
		println c.somar(10, 10)
		println c.getNome()+" - "+c.getData()
	}
	
	@Test
	void exercicio03(){
		// CONSTRUCTOR NAMES ARGUMENTS
		
		Cliente c = new Cliente()
		println c.getNome()+" - "+c.getData()
		
		c = new Cliente(nome: "Rafael")
		println c.getNome()+" - "+c.getData()
		
		c = new Cliente(data: new Date())
		println c.getNome()+" - "+c.getData()
		
		c = new Cliente(nome: "Rafael", data: new Date())
		println c.getNome()+" - "+c.getData()
	}
	
	@Test
	void exercicio04(){
		// SUBSCRIPT OPERATOR
		
		Cliente c = new Cliente(nome: "Jos�", data: new Date())
		println c.getNome()
		println c.getData()
		
		println c["nome"]
		println c["data"]
		
		c["nome"] = "Clodovil"
		println c.getNome()
	}
	
	@Test
	void exercicio05(){
		//DIRECT FIELD ACCESS OPERATOR
		
		Produto p = new Produto("CD", 12.00)
		
		println p.nome
		println p.nome = "CD Calcinha Preta"
		
		p.valor = 15.00
		
		println p.nome+" "+p.valor
	}
	
	@Test
	void exercicio06(){
		// AS
		
		Xu c = new Xu(nome: "Rafael", data: new Date())
		println c.getNome() + " - " + c.getData()
		println c.nome + " - " + c.data
		println ex 
	}
	
	@Test
	void exercicio07(){
		//OPTIONAL PARAMETERS
		
		Venda v = new Venda()
		println v.vender(100)
		println v.vender(100, 15)
	}
	
	@Test
	void exercicio08(){
		// ARRAY OPTIONAL PARAMETERS
		
		Somar soma = new Somar()
		println soma.somar(10)
		println soma.somar(10, 10)
		println soma.somar(10, 10, 10)
	}
	
	@Test
	void exercicio09(){
		// SAFE NAVEGATOR OPERATOR
		
		Cliente c = null
		c?.getNome()
		// � igual a 
		if(c != null){
			c.getNome()
		}
		c?.nome = "Rafael"
		println c?.getNome()
		
		Cliente c2 = new Cliente()
		c2?.getNome()
		// � igual a
		if(c2 != null){
			c2.getNome()
		}
		c2?.nome = "Rafael"
		println c2?.getNome()
		println c2?.nome
	}
	
	@Test
	void exercicio10(){
		// SPREAD OPERATOR
		
		List<String> colecao = new ArrayList()
		colecao.add("Rafael")
		colecao.add(null)
		colecao.add("Vitoria")
		colecao.add("Joserlandersom")
		colecao.add("Manurita")
		println colecao
		
		// em java
		for(String item: colecao){
			if(item != null){
				item = item.toUpperCase()	
			}
		}
		
		// em groovy
		colecao = colecao*.toUpperCase()
		println colecao
		
		colecao = colecao*.replace("A", "@")
		println colecao
	}
	
	@Test
	void exercicio11(){
		// CHECKED EXCEPTIONS S�O OPCIONAIS
		
		// � opcional
		try{
			URL url = new URL("http://www.google.com.br")
		}catch(Exception e){
			e.printStackTrace()
		}		
		
		URL url = new URL("http://www.google.com.br")
		println url
	}
	
	@Test
	void exercicio12(){
		// BOOLEAN AVALUATION
		
		String nome = null
		if(nome){
			println "nome true"
		}else{
			println "nome false"
		}
		
		nome = "Rafa"
		if(nome){
			println "nome true"
		}else{
			println "nome false"
		}
		
		// ================
		
		int valor = 0
		if(valor){
			println "valor true"
		}else{
			println "valor false"
		}
		
		valor = 1		
		if(valor){
			println "valor true"
		}else{
			println "valor false"
		}
		
		// ================
		
		List<String> colecao = new ArrayList()
		if(colecao){
			println "colecao true"
		}else{
			println "colecao false"
		}
		
		colecao.add("Rafa")
		if(colecao){
			println "colecao true"
		}else{
			println "colecao false"
		}
		
		// no java
		if(colecao.size() > 0){
			println "colecao true"
		}else{
			println "colecao false"
		}
	}
	
	@Test
	void  exercicio13(){
		// OPERATOR OVERLOADING
		
		Nota n1 = new Nota(itens: 2, valor: 20)
		Nota n2 = new Nota(itens: 2, valor: 20)
		
		// n3 = n1.plus(n2)
		Nota n3 = n1 + n2
		println n3.valor + " - " + n3.itens
		
		// n1 = n1.next()
		n1++
		println n1.valor + " - " + n1.itens
	}

	@Test
	void exercicio13ponto2(){
		// no java
		BigDecimal val = new BigDecimal(10)
		println val
		
		// no groovy
		BigDecimal vl = 10
		println vl
		
		// no java
		BigDecimal x = vl.add(new BigDecimal(1))
		println x
		
		// no groovy
		vl = vl + 1
		println vl
		
		vl++
		println vl
		
		println vl - 5
		
		Date data = new Date()
		println data
		
		// acrescenta mais um dia
		data++	
		println data
		
		data--
		println data
	}
	
	@Test
	void exercicio13ponto3(){
		// TRAIT
		// criada a classes Pessoa com eran�as multiplas das classes Animal e Lutador
	}
		
	@Test
	void exercicio14(){
		// DESIGN PATTERNS
		// ATS TRANSFORMATIONS (@toString)
		
		Funcionario f = new Funcionario(nome: "Rafael", idade: 10, salario: 1300)
		println f
	}
	
	@Test
	void exercicio15(){
		// DESIGN PATTERNS
		// ATS TRANSFORMATIONS (@EqualsAndHashCode)
		// https://docs.groovy-lang.org/latest/html/api/groovy/transform/EqualsAndHashCode.html
		
		Funcionario2 f2 = new Funcionario2(nome: "Rafael", idade: 10, salario: 10)
		Funcionario2 f3 = new Funcionario2(nome: "Rafael", idade: 10, salario: 10)
		println f2.equals(f3)
		println f3.equals(f2)
	}
	
	@Test
	void exercicio16(){
		// DESIGN PATTERNS
		// ATS TRANSFORMATIONS (@Immutable)
		
		 Pedido p = new Pedido(cliente: "Rafael", numero: 1114)
		 println p.cliente +" - "+ p.numero
		 println p
		 
		 // Immutables n�o � possivel setar, o groovy verifica e estoura um erro
		 // p.cliente = "Raf"
		 // p.setCliente("Raf")
	}
	
	@Test
	void exercicio17(){
		// DESIGN PATTERNS
		// ATS TRANSFORMATIONS (@Singleton)
		
		Conexao.instance.valor = 10
		println Conexao.instance.valor
		Conexao con = Conexao.instance
		con.valor = 11
		println Conexao.instance.valor
		
		// Singletons n�o podem ser instanciados, o groovy verifica e estoura o erro
		// Conexao x = new Conexao()
	}
	
	@Test
	void exercicio18(){
		// DESIGN PATTERNS
		// ATS TRANSFORMATIONS (@Builder)
		// DOCUMENTA��O ATS TRANSFORMATION COMPLETA: https://groovy-lang.org/metaprogramming.html#_available_ast_transformations
		
		Comida comida = Comida.builder().fruta("melancia").bebida("suco de uva").doce("sorvete").build()
		println comida.fruta
		println comida.bebida
		println comida.doce
	}
	
	@Test
	void exercicio19(){
		// OPERADOR DEF
		
		def objeto = "Texto"
		println objeto.getClass()
		
		objeto = 10
		println objeto.getClass()
		
		objeto = 10.00
		println objeto.getClass()
		
		objeto = new Nota(itens: 2, valor: 20)
		println objeto.getClass()
		
		objeto = new Pedido(cliente: "Rafael", numero: 1114)
		println objeto.getClass()
	}
	
	@Test
	void exercicio20(){
		// OPERADOR DEF
		
		def teste = new Teste()
		
		def v1 = teste.metodo("Fael")
		println v1.getClass()
		println v1
		
		v1 = teste.metodo(5)
		println v1.getClass()
		println v1
		
		v1 = teste.metodo(new BigDecimal(6))
		println v1.getClass()
		println v1
		
		def data = new Date()
		println data
		v1 = teste.metodo(data)
		println v1.getClass()
		println v1
	}
	
	@Test
	void exercicio21(){
		// OPERADOR DEF (FOR-IN)
		
		def colecao = new ArrayList<String>()
		colecao.add("Rafa")
		colecao.add("Fael")
		for(item in colecao){
			println item
		}
		
		colecao = new ArrayList<Integer>()
		colecao.add(1)
		colecao.add(2)
		for(item in colecao){
			println item
		}
		
		colecao = "Rafael esta aqui parado"
		for(item in colecao){
			println item
		}
		
		colecao = 1114
		for(item in colecao){
			println item
		}
		
		colecao = new Funcionario(nome: "Rafael", idade: 18, salario: 150)
		for(item in colecao){
			println item
		}
	}
	
	@Test
	void exercicioXX(){
		// CODE COMPLETION FUNCIONA POIS POSSUI UMA TIPAGEM DEFINIDA
		ArrayList<String> objeto = new ArrayList<String>()
		objeto.add("Rafa")
		println objeto
		
		// N�O CONFIAR MAIS NO CODE COMPLETION
		// CODE COMPLETION N�O FUNCIONA POIS N�O POSSUI UMA TIPAGEM PR� DEFINIDA
		def x = new ArrayList<>()
		x.add("Farrael")
		println x
		
		x = "Mudou rafael"
		x.toUpperCase()
		println x
	}
}

/*
REFERENCIA BIBLIOGRAFICA
LIVROS:
	PROGRAMMING GROOVY 2 - VENKAT SUBRAMANIAM (MELHOR LIVRO PARA COME�AR)
	MAKING JAVA GROOVY - ????	(PRECISA TER UM CONHECIMENTO DO GROOVY)
	GROOVY IN ACTION - ???? (PR�TICO, PARTE DO SUPOSTO QUE VOCR J� SAIBA GROOVY)	
*/