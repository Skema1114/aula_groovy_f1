package classes

import groovy.transform.ToString

/* no groovy para gerar o toString
// @ToString

// ELE N�O MOSTRA O CAMPO INFORMADO, NAO GERA O TOSTRING DO CAMPO
// @ToString(excludes=["idade"])

// ELE INCLUI O NOME DOS CAMPOS ANTES DO VALOR
// @ToString(includeNames=true)

// DA PARA USAR UNI�ES DE CHAMADAS */
@ToString(includeNames=true, excludes="idade, salario")
class Funcionario {
	String nome
	Integer idade
	Double salario	
	// no java para gerar o toString
    //	@Override 
    //	public String toString() {
    //		return "Funcionario [nome=" + nome + ", idade=" + idade + ", valor=" + valor + "]";
    //	}
}
