package classes;

import java.util.Date;

public class ClienteJava {
	private String nome;
	private Date data;
	
	// euivale ao exercicio03
	public ClienteJava(String p1){
		nome = p1;
	}
	
	public ClienteJava(Date p1){
		data = p1;
	}
	
	public ClienteJava(String p1, Date p2){
		nome = p1;
		data = p2;
	}
	// // //
	
	public Integer somar(Integer v1, Integer v2){
		return v1 + v2;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
}
