package classes

// � uma interface e uma classe abstrata

trait Animal {
	String nome
	
	abstract void pular()
	
	void falar(){
		println "Animal "+ nome +" falando..."
	}
}
