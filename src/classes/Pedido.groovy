package classes

import groovy.transform.Immutable

/* gerando com groovy */
@Immutable
class Pedido {
	String cliente
	Integer numero
	/*
	no java
	
	public String getCliente() {
		return cliente;
	}
	
	public Integer getNumero() {
		return numero;
	}

	@Override
	public String toString() {
		return "Pedido [cliente=" + cliente + ", numero=" + numero + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pedido other = (Pedido) obj;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		return true;
	}
	*/
}